resource "kubernetes_config_map" "backend_app" {
  metadata {
    name = "backend-app"
    namespace = var.ada_namespace
  }
  data = {
    MYSQL_DB_HOST = "jdbc:mysql://${aws_db_instance.db_instance.endpoint}/${aws_db_instance.db_instance.db_name}?autoReconnect=true&useSSL=false"
    MYSQL_DB_USER = aws_db_instance.db_instance.username
    MYSQL_DB_PASS = aws_db_instance.db_instance.password
  }
  depends_on = [
    kubernetes_namespace.ada_apps
  ]
}